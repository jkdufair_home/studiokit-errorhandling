﻿using Newtonsoft.Json.Linq;
using SharpRaven.Data;

namespace StudioKit.ErrorHandling
{
	public class DefaultJsonPacketFactory : JsonPacketFactory
	{
		protected override JsonPacket OnCreate(JsonPacket jsonPacket)
		{
			var jObject = jsonPacket?.Extra as JObject;
			if (jObject == null)
				return base.OnCreate(jsonPacket);

			// get SentryUser object from the "extra" data dictionary
			// if it exists, move the user details to the jsonPacket.User
			var user = jObject["user"]?.ToObject<ExceptionUser>();
			if (user != null)
			{
				jsonPacket.User = new SentryUser(user.UserName)
				{
					Id = user.Id,
					Email = user.Email,
					Username = user.UserName
				};
			}

			// get any other "real" extra data
			var extraObject = jObject["extra"];
			jsonPacket.Extra = extraObject;

			return base.OnCreate(jsonPacket);
		}
	}
}