﻿using SharpRaven;
using SharpRaven.Data;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StudioKit.ErrorHandling
{
	/// <summary>
	/// Error Handler implementation which sends exceptions to Sentry.
	///
	/// Inject a custom <see cref="IJsonPacketFactory"/> that overrides
	/// the final <see cref="JsonPacket"/>s that are sent to Sentry.
	///
	/// The <see cref="Client"/> gives access to
	/// other Sentry functionality.
	///
	/// </summary>
	public class SentryErrorHandler : IErrorHandler
	{
		private static readonly List<Regex> ExcludeRegexPatterns = new List<Regex>
		{
			new Regex(@"A public action method.*was not found on controller.*"),
			new Regex(@"The controller for path.*was not found or does not implement IController."),
			new Regex(@"A potentially dangerous Request.Path value was detected from the client.*")
		};

		public RavenClient Client { get; set; }

		public SentryErrorHandler(string dsn, IJsonPacketFactory jsonPacketFactory)
		{
			if (string.IsNullOrWhiteSpace(dsn))
				return;
			Client = new RavenClient(dsn, jsonPacketFactory);
		}

		public void CaptureException(Exception ex)
		{
			CaptureException(ex, null, null);
		}

		public void CaptureException(Exception ex, IExceptionUser user)
		{
			CaptureException(ex, user, null);
		}

		public void CaptureException(Exception exception, IExceptionUser user, object extra)
		{
			if (ExcludeRegexPatterns.Any(r => r.IsMatch(exception.Message)))
				return;
			Client?.Capture(new SentryEvent(exception)
			{
				// pass user and extra data inside a dictionary, so the JsonPacketFactory can handle it
				Extra = new Dictionary<string, object>
				{
					{"user", user},
					{"extra", extra}
				}
			});
		}
	}
}