﻿using Microsoft.AspNet.Identity;
using StudioKit.ErrorHandling.Interfaces;
using System.Security.Claims;
using System.Security.Principal;

namespace StudioKit.ErrorHandling
{
	public static class PrincipalIdentityExtensions
	{
		public static IExceptionUser ToExceptionUser(this IIdentity identity)
		{
			var claimsIdentity = identity as ClaimsIdentity;
			return new ExceptionUser
			{
				Id = identity.GetUserId(),
				UserName = identity.GetUserName(),
				Email = claimsIdentity?.FindFirstValue(ClaimTypes.Email)
			};
		}
	}
}